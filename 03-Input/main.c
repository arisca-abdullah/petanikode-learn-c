#include <stdio.h>

void main() {
    // Make a variable to store the data
    char name[32], email[48];

    // fgets function
    printf("Name: ");
    fgets(name, sizeof(name), stdin);
    
    // scanf function
    printf("Email: ");
    scanf("%[^\n]s", email);

    printf("--------------------------------------\n");

    printf("Your name: %s", name);
    printf("Your email: %s\n", email);
}