#include <stdio.h>

void main() {
    // Show text with format
    printf("Hello, this is is an output text\n");
    printf("My name is %s\n", "Aris");
    printf("I am %d years old\n", 17);

    // This is a comment
    // %c       to show character
    // %s       to show text(string)
    // %d, %i   to show decimal number
    // %f       to show float number
    // %o       to show octal number
    // %x       to show hexadecimal number
    // \t       to create tabs

    // Show text without format
    puts("Hello, World!");
    puts("My name is Aris");

    // Show a character
    putchar('A');
}