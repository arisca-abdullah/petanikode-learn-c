#include <stdio.h>

void main() {
    // Make an enum
    enum level { EASY = 1, NORMAL = 2, HARD = 3 };
    enum boolean { false, true };

    // Accessing enum value
    printf("Enemy level: %i\n", HARD);
    if(HARD > NORMAL == true) {
        printf("Level %i is harder than level %i\n", HARD, NORMAL);
    } else {
        printf("Level %i isn't harder than level %i\n", HARD, NORMAL);
    }
}