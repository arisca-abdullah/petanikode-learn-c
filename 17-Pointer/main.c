#include <stdio.h>

void main() {
    int a = 5;

    // Make a pointer
    int *pa = &a;

    printf("Memory address of variable a: %p\n", &a);
    printf("Memory address of pointer pa: %p\n", pa);

    printf("Value of variable a: %i\n", a);
    printf("Value of pointer pa: %i\n", *pa);

    // Change variable value by pointer
    *pa = 10;

    printf("Value of variable a: %i [changed]\n", a);
    printf("Value of pointer pa: %i [changed]\n", *pa);
}