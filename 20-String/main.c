#include <stdio.h>
#include <string.h>

void main() {
    char hello[] = "Hello";
    char hello_copy[20];

    printf("hello       : %s\n", hello);

    // Copy string from a variable to other variable
    strcpy(hello_copy, hello);
    printf("hello_copy  : %s\n", hello_copy);

    // Concatenation
    strcat(hello, ", World!");
    printf("hello       : %s\n", hello);

    // String length
    int hello_length = strlen(hello);
    printf("hello length: %i\n", hello_length);

    // Compare string
    int is_same = strcmp(hello, hello_copy);
    printf("hello and hello_copy is %s\n", is_same == 1 ? "same" : "different");

    // Find a character inside string
    char *res;
    res = strchr(hello, 'H');
    printf("hello %s character 'H'\n", res != NULL ? "contains" : "doesn't contain");

    // Find string inside string
    char *res2;
    res2 = strstr(hello, "World");
    printf("hello %s \"World\"\n", res2 != NULL ? "contains" : "doesn't contain");
}