# Hello!
 I am Aris and I am learning C programming language.

## Why did I learn C language?
Most programmers start learning programming language with C. Therefore, I am interested and I want to try the C language even though I have previously learned programming language using other programming language. 

## What have I learned? 
1. Hello, World!
2. Input and Output
3. Variable, Data types, and constanta
4. Operators
5. Branching (if else, switch case)
6. Looping (for, while, do while)
7. Array
8. Function
9. Pointer
10. Enum
11. Struct
12. String