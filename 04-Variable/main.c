#include <stdio.h>

void main() {
    // How to make a variable

    // 1. Declaration
    int height;
    // Assignment
    height = 174;

    // 2. Initialization
    int weight = 58;

    // Show output
    printf("My height is %i cm and my weight is %i kg\n", height, weight);
}