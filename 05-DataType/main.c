#include <stdio.h>

void main() {
    // 1. interger -> to store a decimal number
    int age = 17;
    
    // 2. float -> to store a float number
    float weight = 58.5;

    // 3. double -> like float but has larger capacity
    double height = 174.35;

    // 4. char -> to store a character
    char gender = 'M';

    printf("Your age: %i\n", age);
    printf("Your weight: %.1f\n", weight);
    printf("Your height: %.2f\n", height);
    printf("Your gender: %c\n", gender);
}