#include <stdio.h>

void main() {
    int a = 15;
    int b = 10;

    float result = (float) a / (float) b;

    printf("%i / %i = %.1f\n", a, b ,result);
}