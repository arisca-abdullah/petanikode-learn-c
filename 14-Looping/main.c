#include <stdio.h>
#include <ctype.h>

void main() {
    // For
    puts("----- FOR -----");

    for(int i = 0; i < 3; i++) {
        printf("Looping %i\n", i + 1);
    }

    // While
    puts("---- WHILE ----");

    int counter = 0;
    while(counter < 3) {
        printf("Looping %i\n", counter + 1);
        counter++;
    }

    // Do while
    puts("--- DO WHILE ---");

    counter = 0;
    do {
        printf("Looping %i\n", counter + 1);
        counter++;
    } while(counter < 3);
}