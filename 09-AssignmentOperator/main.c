#include <stdio.h>

void main() {
    int number;

    // Assignment
    number = 10;
    printf("Number = %i\n", number);

    // Assignment and addition
    number += 10;
    printf("Number += 10 is %i\n", number);

    // Assignment and substraction
    number -= 5;
    printf("Number -= 5 is %i\n", number);

    // Assignment and multiplication
    number *= 3;
    printf("Number *= 3 is %i\n", number);

    // Assignment and division
    number /= 5;
    printf("Number /= 5 is %i\n", number);

    // Others operators
    // %=   -> Assignment and modulo
    // <<=  -> Assignment and left shift
    // >>=  -> Assignment and right shift
    // &=   -> Assignment and bitwise AND
    // |=   -> Assignment and bitwise OR
    // ^=   -> Assignment and bitwise XOR
}