#include <stdio.h>

void main() {
    int a;
    printf("Enter a number: ");
    scanf("%i", &a);

    // * to make a pointer
    // & to get memory address from a variable
    int *pa = &a;

    printf("Memory address of variable a is %p\n", pa);

    // Ternary operator ... ? ... : ...
    printf("%i is %s\n", a, a % 2 == 0 ? "even" : "odd");

    // Increment
    a++;
    printf("a++ = %i\n", a);
    ++a;
    printf("++a = %i\n", a);

    // Decrement
    a--;
    printf("a-- = %i\n", a);
    --a;
    printf("--a = %i\n", a);
}