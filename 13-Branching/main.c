#include <stdio.h>
#include <ctype.h>

void main() {
    int a;
    char grade;

    printf("Enter your mark: ");
    scanf("%d", &a);

    // If
    if(a == 100) {
        puts("You got a perfect mark!");
    }

    // If else
    if(a <= 100 && a >= 80) {
        puts("You passed the test.");
    } else {
        puts("You have to do the test again!");
    }

    // Else if
    if(a > 100) {
        puts("Your grade is invalid!");
    } else if(a >= 90) {
        grade = 'A';
        printf("Your grade is %c.\n", grade);
    } else if(a >= 80) {
        grade = 'B';
        printf("Your grade is %c.\n", grade);
    } else {
        grade = 'C';
        printf("Your grade is %c.\n", grade);
    }

    // Switch case
    switch(toupper(grade)) {
        case 'A':
            puts("You got a great mark!");
            break;
        case 'B':
            puts("You got a good mark!");
            break;
        case 'C':
            puts("You got a bad mark!");
            break;
        default:
            puts("You got an invalid mark!");
    }
}