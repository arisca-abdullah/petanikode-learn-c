#include <stdio.h>

// Global variable
int number = 5;

// Create a function
void greet() {
    puts("Hello, World!");
}

// Function with parameter
void square(int a) {
    printf("%i squared is %i\n", a, a * a);
}

// Function with return value
int cubic(int a) {
    int result = a * a * a;
    return result;
}

// Recursive function
int factorial(int a) {
    if(a < 0) return 0;
    if(a == 0) return 1;
    return a * factorial(a - 1);
}

// Pointer as parameter
void plusFive(int *a) {
    *a = *a + 5;
}

void main() {
    greet();

    // Pass by value
    square(4);
    printf("3 cubic is %i\n", cubic(3));
    printf("Factorial 5 is %i\n", factorial(5));

    printf("Number: %i\n", number);
    // Pass by reference
    plusFive(&number);
    printf("Number: %i [changed]\n", number);
}