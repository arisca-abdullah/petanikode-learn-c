#include <stdio.h>

// Make a struct
struct rectangle {
    int width;
    int height;
};

// Using typedef keyword to create a new data type
typedef struct address {
    char *state;
    char *region;
    char *city;
} addr;

// Nested struct
typedef struct person {
    char *name;
    char *dob;
    // Using a new datatype created with struct
    addr address;
} person;

// Using struct as parameter
int getRectArea(struct rectangle a) {
    return a.width * a.height;
};

void main() {
    printf("-------- RECT1 ---------\n");

    // Using struct
    struct rectangle rect1;
    rect1.width = 40;
    rect1.height = 100;

    printf("Width : %i\n", rect1.width);
    printf("Height: %i\n", rect1.height);
    // Passing struct as argumen
    printf("Area  : %i\n", getRectArea(rect1));

    printf("-------- PERSON1 --------\n");

    // Using a new datatype created with struct
    person person1;
    person1.name = "Adam";
    person1.dob = "31 Dec 2012";
    person1.address.state = "Indonesia";
    person1.address.region = "West Java";
    person1.address.city = "Bandung";

    printf("Name            : %s\n", person1.name);
    printf("Date of Birth   : %s\n", person1.dob);
    printf("State           : %s\n", person1.address.state);
    printf("Region          : %s\n", person1.address.region);
    printf("City            : %s\n", person1.address.city);
}