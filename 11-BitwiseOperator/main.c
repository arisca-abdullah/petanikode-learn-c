#include <stdio.h>

void main() {
    int a = 6; // 0110
    int b = 3; // 0011

    // Bitwise AND
    printf("%i & %i = %i\n", a, b, a & b); // 0010 -> 2

    // Bitwise OR
    printf("%i & %i = %i\n", a, b, a | b); // 0111 -> 7

    // Bitwise XOR
    printf("%i ^ %i = %i\n", a, b, a ^ b); // 0101 -> 5

    // Bitwise NOT
    printf("~%i = %i\n", a, ~a); // 1001 -> -7

    // Bitwise Left Shift
    printf("%i << 1 = %i\n", a, a << 1); // 1100 -> 12

    // Bitwise Right Shift
    printf("%i >> 1 = %i\n", a, a >> 1); // 0011 -> 3
}