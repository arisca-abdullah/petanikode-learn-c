#include <stdio.h>

void main() {
    // Make an array with length 5 and fill it
    char alphabet[2] = {'a', 'b'};

    // Access array value by index
    printf("Alphabet 1: %c\n", alphabet[0]);
    printf("Alphabet 2: %c\n", alphabet[1]);

    // Change array value
    alphabet[1] = 'z';
    printf("Alphabet 2: %c [changed]\n", alphabet[1]);

    printf("-----------------------------\n");

    // Make an array with length 5
    int marks[5];

    // Fill the array
    marks[0] = 78;
    marks[1] = 84;
    marks[2] = 88;
    marks[3] = 90;
    marks[4] = 80;

    // Get array length
    int marksLength = sizeof(marks) / sizeof(*marks);

    int total = 0;
    for(int i = 0; i < marksLength; i++) {
        printf("Mark %i: %i\n", i + 1, marks[i]); // Show mark by index
        total += marks[i]; // sum and assign mark to total variable
    }

    printf("Total marks: %i\n", total);
    printf("Marks count: %i\n", marksLength);
    printf("Average marks is %f\n", (float) total / (float) marksLength);

    printf("--- Multidimensional Array ---\n");

    // Make a multidimensional array
    int matrix[2][2] = {
        {1, 0},
        {0, 1}
    };

    // Show array values
    for(int i = 0; i < (sizeof(matrix) / sizeof(*matrix)); i++) {
        for(int j = 0; j < (sizeof(matrix[i]) / sizeof(*matrix[i])); j++) {
            printf("Value of array index (%i, %i): %i\n", i, j, matrix[i][j]);
        }
    }
}