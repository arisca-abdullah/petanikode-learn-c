#include <stdio.h>
#define PI 3.14

void main () {
    const int ONLINE = 1;

    printf("Value of PI is %.2f\n", PI);
    printf("Value of ONLINE is %i\n", ONLINE);
}