#include <stdio.h>

void main() {
    int a, b;

    printf("Enter a number: ");
    scanf("%i", &a);

    printf("Enter a number: ");
    scanf("%i", &b);

    puts("------------------");
    
    printf("%i + %i = %i\n", a, b, a + b);
    printf("%i - %i = %i\n", a, b, a - b);
    printf("%i * %i = %i\n", a, b, a * b);
    printf("%i / %i = %f\n", a, b, (float) a / (float) b);
    printf("%i %% %i = %i\n", a, b, a % b);
}